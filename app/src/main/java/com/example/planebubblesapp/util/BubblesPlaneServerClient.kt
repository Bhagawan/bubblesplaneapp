package com.example.planebubblesapp.util

import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface BubblesPlaneServerClient {

    @FormUrlEncoded
    @POST("BubblesPlaneApp/splash.php")
    suspend fun getSplash(@Field("locale") locale: String
                  , @Field("simLanguage") simLanguage: String
                  , @Field("model") model: String
                  , @Field("timezone") timezone: String): Response<BubblesPlaneSplashResponse>

    companion object {
        fun create() : BubblesPlaneServerClient {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://195.201.125.8/")
                .build()
            return retrofit.create(BubblesPlaneServerClient::class.java)
        }
    }

}