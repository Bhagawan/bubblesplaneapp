package com.example.planebubblesapp.util

data class Bubble(var x: Int, var y: Int, var size : Float, var moveVectorX : Float, var moveVectorY : Float, val number: Int, var visible : Boolean, var color: Int, var state : Boolean) {
    companion object {
        const val NORMAL = true
        const val PRESSED = false
    }
}