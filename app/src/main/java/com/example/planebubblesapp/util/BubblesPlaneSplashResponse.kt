package com.example.planebubblesapp.util

import androidx.annotation.Keep

@Keep
data class BubblesPlaneSplashResponse(val url : String)