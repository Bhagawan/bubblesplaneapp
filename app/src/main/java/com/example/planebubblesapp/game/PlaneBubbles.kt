package com.example.planebubblesapp.game

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.toBitmap
import com.example.planebubblesapp.R
import com.example.planebubblesapp.util.Bubble
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.concurrent.fixedRateTimer
import kotlin.math.*
import kotlin.random.Random

class PlaneBubbles(context: Context, attributeSet: AttributeSet?) : View(context, attributeSet) {
    private var mWidth = 0
    private var mHeight = 0
    private var planeX = 0.0f
    private var planeY = 0.0f
    private var planeAngle = 0.0f

    private var dX = 0.0f
    private var dY = 0.0f
    private var up = true

    private val plane = BitmapFactory.decodeResource(context.resources, R.drawable.plane)
    private val restart = AppCompatResources.getDrawable(context,R.drawable.ic_baseline_replay)?.toBitmap()
    private val bubble = BitmapFactory.decodeResource(context.resources, R.drawable.bubble)

    private var state = START

    private var bubblesTimer = 40
    private val bubbles = List(6){ n -> Bubble(0,0, bubbleWidth, 0.0f, 0.0f, n + 1, false, Color.WHITE, Bubble.NORMAL) }
    private var bubbleWidth = 0.0f

    companion object {
        const val START = 0
        const val FLYING = 1
        const val FINISH = 2
    }

    init {
        fixedRateTimer("refresh", false, 0, 1000 / 60) {
            CoroutineScope(Dispatchers.Main).launch { invalidate() }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if(w > 0) {
            mWidth = w - paddingStart - paddingEnd
            mHeight = h - paddingTop - paddingBottom
            planeY = mHeight / 3.0f
            bubbleWidth = mWidth * 0.1f
        }
    }

    override fun onDraw(canvas: Canvas?) {
        canvas?.let {
            if(state == FLYING) updatePlane()
            drawPlane(it)
            drawGround(it)
            drawUI(it)
            moveBubbles()
            drawBubbles(it)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when(event?.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
                when(state) {
                    START -> {

                    }
                    FLYING -> {

                    }
                    FINISH -> {

                    }
                }
                return true
            }
            MotionEvent.ACTION_MOVE -> {
                when(state) {
                    START -> {
                    }
                    FLYING -> {
                    }
                    FINISH -> {
                    }
                }
                return true
            }
            MotionEvent.ACTION_UP -> {
                when(state) {
                    START -> {
                        dX = 1.0f
                        dY = 0.0f
                        state = FLYING
                    }
                    FLYING -> {
                        checkPress(event.x, event.y)
                    }
                    FINISH -> {
                        restart()
                    }
                }
                return true
            }
        }
        return super.onTouchEvent(event)
    }

    private fun drawPlane(c: Canvas) {
        val rotationMatrix = Matrix()
        rotationMatrix.postRotate(planeAngle)
        val rotatedPlane = Bitmap.createBitmap(plane, 0,0,plane.width, plane.height, rotationMatrix, true )
        val p = Paint()
        p.color = Color.WHITE
        c.drawBitmap(rotatedPlane, 0.0f, planeY.coerceAtLeast(mHeight * 0.2f), p)
    }

    private fun drawGround(c : Canvas) {
        if(planeY >= -20) {
            val p = Paint()
            p.color = Color.WHITE
            p.strokeWidth = 3.0f
            val y =(mHeight - planeY - 50.0f).coerceAtLeast(mHeight - 20.0f)
            c.drawLine(0.0f, y, mWidth.toFloat(), y, p)
        }
    }

    private fun drawUI(c: Canvas) {
        val p = Paint()
        p.color = Color.WHITE
        p.strokeWidth = 3.0f
        p.textSize = 60.0f
        p.textAlign = Paint.Align.CENTER

        when(state) {
            START -> {
                p.color = ResourcesCompat.getColor(context.resources, R.color.grey, null)
                val l = (mWidth - 21 * 40) / 2.0f
                val t = (mHeight - 200) / 2.0f
                c.drawRoundRect(l, t, mWidth - l, mHeight - t, 20f, 20f, p)
                p.color = Color.BLACK
                c.drawText("Лопайте пузыри,", mWidth / 2.0f, mHeight / 2.0f - 30, p)
                c.drawText("в правильном порядке", mWidth / 2.0f, mHeight / 2.0f + 30, p)
            }
            FLYING -> {
                c.drawText("Дистанция: ${planeX / 10} м", mWidth / 2.0f, mHeight * 0.07f, p)
            }
            FINISH -> {
                p.color = ResourcesCompat.getColor(context.resources, R.color.grey, null)
                c.drawRoundRect(mWidth * 0.15f, mHeight * 0.3f - 100, mWidth * 0.85f, mHeight * 0.4f, 20f, 20f, p)
                val pad = (restart?.width ?: 90) * 0.5f + 5
                c.drawRoundRect(mWidth * 0.5f - pad, mHeight * 0.7f - pad, mWidth * 0.5f + pad, mHeight * 0.7f + pad, 20f, 20f, p)

                p.color = Color.BLACK
                c.drawText("Самолет пролетел: ${planeX / 10} м", mWidth * 0.5f, mHeight * 0.3f, p)

                p.style = Paint.Style.STROKE
                p.color = Color.WHITE
                c.drawRoundRect(mWidth * 0.5f - pad + 5, mHeight * 0.7f - pad + 5, mWidth * 0.5f + pad - 5, mHeight * 0.7f + pad - 5, 20f, 20f, p)

                restart?.let { c.drawBitmap(restart, (mWidth - restart.width) * 0.5f, mHeight * 0.7f - restart.height * 0.5f, p) }
            }
        }
    }

    private fun updatePlane() {
        dY += 0.005f * if(up) -1 else 1
        if(dY <= -1) up = false

        val newY = planeY + dY * (dY.absoluteValue * 10)

        planeAngle =  Math.toDegrees((sign(dY) * acos(dX / sqrt(dX.pow(2) + dY.pow(2)))).toDouble()).toFloat()
        planeY = newY
        planeX += dX
        if(bubblesTimer > 0) bubblesTimer--
        else if(bubblesTimer == 0) showBubbles()

        if(planeY >  mHeight - 50 -  plane.height * 0.6f) {
            state = FINISH
            for (b in bubbles) b.visible = false
        }
    }

    private fun restart() {
        state = START
        planeX = 0.0f
        planeY = mHeight / 3.0f
        planeAngle = 0.0f
        bubblesTimer = 40
    }

    private fun drawBubbles(c: Canvas) {
        val p = Paint()
        p.textAlign = Paint.Align.CENTER
        p.textSize = bubbleWidth * 0.5f

        for(b in bubbles) {
            if(b.visible) {
                p.colorFilter = LightingColorFilter(b.color, 1)
                if(b.state == Bubble.NORMAL) {
                    c.drawBitmap(bubble,null, Rect(b.x, (b.y - b.size).toInt(), (b.x + b.size).toInt(), b.y), p)
                    p.color = Color.parseColor("#476491")
                    c.drawText(b.number.toString(), b.x + b.size * 0.5f, b.y - b.size * 0.3f, p)
                } else if(b.size > 5.0f) {
                    b.size--
                    val x = b.x + (bubbleWidth * 0.5f)
                    val y = b.y - (bubbleWidth * 0.5f)
                    c.drawBitmap(bubble,null, Rect((x - b.size * 0.5f).toInt(), (y - b.size * 0.5f).toInt(), (x + b.size * 0.5f).toInt(),(y + b.size * 0.5f).toInt()), p)
                } else b.visible = false
            }
        }
    }

    private fun showBubbles() {
        bubblesTimer = -1
        for(b in bubbles) {
            b.x = Random.nextInt(width / 2, width - bubbleWidth.toInt())
            b.y = Random.nextInt(bubbleWidth.toInt(), height - bubbleWidth.toInt())
            b.moveVectorX = Random.nextFloat() * Random.nextInt(-1, 1) * 5
            b.moveVectorY = Random.nextFloat() * Random.nextInt(-1, 1) * 5
            b.visible = true
            b.color = Color.WHITE
            b.state = Bubble.NORMAL
            b.size = bubbleWidth
        }
    }

    private fun moveBubbles() {
        if(bubblesTimer < 0) {
            for(b in bubbles) {
                if(b.x + b.moveVectorX !in (width * 0.5f)..(width - bubbleWidth)) b.moveVectorX *= -1
                if(b.y + b.moveVectorY !in bubbleWidth..(height.toFloat() - bubbleWidth)) b.moveVectorY *= -1
                b.x += b.moveVectorX.toInt()
                b.y += b.moveVectorY.toInt()
            }
        }
    }

    private fun checkPress(x: Float, y: Float) {
        for(b in bubbles) {
            if(b.state == Bubble.NORMAL) {
                if(x in b.x.toFloat()..(b.x + b.size) && y in (b.y - b.size)..b.y.toFloat() ) {
                    if(bubbles.indexOf(b) == bubbles.size - 1) {
                        bubblesTimer = 40
                        pushPlane()
                    }
                    b.state = Bubble.PRESSED
                    b.color = Color.GREEN
                    break
                } else {
                    miss()
                    break
                }
            }
        }
    }

    private fun miss() {
        bubblesTimer = 40
        for(b in bubbles) {
            if(b.visible && b.state == Bubble.NORMAL) {
                b.state = Bubble.PRESSED
                b.color = Color.RED
            }
        }
    }

    private fun pushPlane() {
        up = true
    }

}